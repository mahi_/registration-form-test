import Vue from 'vue'
import Vuex from 'vuex'
import createPersist from 'vuex-localstorage'
import jwt from 'jsonwebtoken'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    // createPersist({
    //   namespace: 'app-store',
    //   initialState: {},
    //   // expires: 1 * 24 * 60 * 60 * 1e3 // ONE_DAY
    // })
  ],
  state: {
    profileObj: {}
  },
  mutations: {
    setProfileObj (state, obj) {
      state.profileObj = obj
    }
  },
  actions: {
    setProfileObj ({ commit }, data) {
      commit('setProfileObj', data)
    }
  },
  getters: {
    getProfileObj: state => {
      return state.profileObj
    }
  }
})
