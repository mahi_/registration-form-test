import Vue from 'vue'
import Router from 'vue-router'
import store from './store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

router.beforeEach(async (to, from, next) => {
  const isAuthenticated = store.getters.isAuthenticated
  const isSessionAvailable = store.getters.isSessionAvailable
  const el = document.body
  el.removeAttribute('class')
  const className = 'page-' + to.name || from.name
  el.classList.add(className)
  if (to.name !== 'login' && to.meta.requiresAuth) {
    if (isSessionAvailable && isAuthenticated) {
      next()
    } else if (
      (!isAuthenticated && isSessionAvailable) ||
      (isAuthenticated && !isSessionAvailable)
    ) {
      // set the last accessing page link to store and redirect to that page after login
      await store.dispatch('setPreviousPageLink', to.path)
      return next('/session-expiry')
    } else {
      // if not logged in and try visiting any page, set the link to store and redirect to that page after login
      await store.dispatch('setPreviousPageLink', to.path)
      return next('/login')
    }
  } else if (isAuthenticated && to.name === 'login') {
    // if already logged in and visiting the login page redirecting to dashboard
    next('/dashboard')
  } else {
    if (to.name === 'session-expiry') {
      await store.dispatch('setPreviousPageLink', from.path)
    }
    next()
  }
  // from.path.split('/')[1] &&  to.path.split('/')[1] &&
  const fromUrl = from.path.split('/')[1]
  const toUrl = to.path.split('/')[1]
  if (!fromUrl || !toUrl || fromUrl !== toUrl) {
    store.dispatch('setSearchNull', {})
  }
  store.commit('setActiveHeaderButton', to.path.split('/')[1])
  if (
    to.path.includes('/pdf-view') ||
    to.path.includes('/image-view') ||
    to.path.includes('/enquiry/print')
  ) {
    store.commit('showHeader', false)
  } else {
    store.commit('showHeader', true)
  }
  store.commit('setMenuIcon', false)
})
export default router
